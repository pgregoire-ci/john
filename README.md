# john (bleeding jumbo) binaries

## Runtime dependencies

```
# Debian buster
libbz2-1.0
libogmp1
libpcap0.8
ocl-icd-libopencl1

libencode-perl
python
python-dpkt
python-scapy
```

https://gitlab.com/pgregoire-ci/john/-/jobs/artifacts/main/raw/john-bleeding-jumbo.tar.gz?job=build


## Known bugs

We are aware of a packaging issue where john-specific Perl modules
are not part of the package. See, https://github.com/openwall/john/issues/4089
for a similar issue that also affects this package.
