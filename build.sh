#!/bin/sh
set -eux

tmpdir=$(mktemp -d)
cd "${tmpdir}"

apt-get update
apt-get -y install \
	gcc \
	git \
	libgmp-dev \
	libbz2-dev \
	libpcap-dev \
	libssl-dev \
	ocl-icd-opencl-dev \
	opencl-headers \
	make \
	pkg-config \
	pocl-opencl-icd \
	sudo \
	yasm \
	zlib1g-dev \


git clone https://github.com/openwall/john -b bleeding-jumbo --depth=1
cd john/src

grep '^[[:space:]]' ../doc/README-DISTROS | bash -x

cd ..
cp -r run/rules/ /usr/local/share/john/rules/
[ -d /usr/local/share/doc/ ] || mkdir /usr/local/share/doc/
cp -r doc /usr/local/share/doc/john

tar --owner 0 --group 0 -f- -c \
	/etc/bash_completion.d/john.bash_completion \
	/usr/local/share/john/ \
	/usr/local/bin/ \
| gzip -c9 >"${1}/john-bleeding-jumbo.tar.gz"
